package com.example.rolldice

data class Player (
    val name: String,
    var dice: Int
)