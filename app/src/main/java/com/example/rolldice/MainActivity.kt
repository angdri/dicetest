package com.example.rolldice

import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View.GONE
import android.view.View.VISIBLE
import android.widget.Toast
import androidx.annotation.RequiresApi
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), View {
    private lateinit var presenter: Presenter
    private lateinit var playerName: String
    private var listPlayer: MutableList<Player> = mutableListOf()
    private var nTime : Int = 0
    private var nPlayerMax = 0
    @RequiresApi(Build.VERSION_CODES.N)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        presenter = Presenter()
        presenter.setView(this)

        btn_roll.setOnClickListener {
            nPlayerMax = n_player.text.toString().toInt()
            et_player_name.visibility = VISIBLE
            n_player.visibility = GONE
            btn_login.visibility = VISIBLE
            btn_roll.visibility = GONE
            tv_result.text = "Masukkan nama player pertama :"
            Toast.makeText(this, "Masukkan nama player pertama", Toast.LENGTH_SHORT).show()
        }

        btn_login.setOnClickListener {
            nTime++
            playerName = et_player_name.text.toString()
            if(listPlayer.contains(Player(playerName,0))){
                Toast.makeText(this, "Nama player sudah ada, silahkan masukkan nama yang berbeda", Toast.LENGTH_LONG).show()
            }else{
                if(nTime < nPlayerMax){
                    addPlayerName(playerName)
                    tv_result.text = "Masukkan nama player ke-${nTime+1}: "
                    Toast.makeText(this, "Masukkan nama player ke-${nTime+1}", Toast.LENGTH_SHORT).show()
                }else{
                    addPlayerName(playerName)
                    presenter.rollDice(listPlayer)
                    et_player_name.visibility = GONE
                    n_player.visibility = VISIBLE
                    btn_login.visibility = GONE
                    btn_roll.visibility = VISIBLE
                    nTime = 0
                    listPlayer.clear()
                }
            }
        }
    }

    override fun showResult(name: String, score: Int) {
        tv_result.text = "$name menang dengan score $score"
        Toast.makeText(this, "$name menang dengan score $score", Toast.LENGTH_LONG).show()
    }

    fun addPlayerName(playerName:String){
        val player = Player(playerName, 0)
        listPlayer.add(player)
    }
}