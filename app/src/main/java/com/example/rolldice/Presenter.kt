package com.example.rolldice

import android.os.Build
import android.util.Log
import androidx.annotation.RequiresApi

class Presenter {
    private lateinit var view: View
    val dice = listOf<Int>(1, 2, 3, 4, 5, 6)

    fun setView(view: View) {
        this.view = view
    }

    @RequiresApi(Build.VERSION_CODES.N)
    fun rollDice(listPlayer: List<Player>) {

        for (player in listPlayer){
            var score = 0
            for (i in 0..5){
                if (dice.random() % 2 == 0) {
                    score -= 3
                }else{
                    score += 5
                }
            }
            player.dice = score
        }
        val maxScore = listPlayer.sortedByDescending{ it.dice }
        view.showResult(maxScore[0].name, maxScore[0].dice)
        Log.d("rollDice", maxScore.toString())
    }
}