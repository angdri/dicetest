package com.example.rolldice

interface View {
    fun showResult(name:String, score:Int)
}